<?php

$db = require __DIR__ . '/db.php'; // By CI generating

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru',
    'bootstrap' => ['log'],
    'components' => [
        'db' => $db,
        'urlManager' => [
            'class' => \modules\core\common\components\UrlManager::class, // separate url configuring
            'managers' => [
                'backend' => [ // admin side
                    'enablePrettyUrl' => true,
                    'showScriptName' => false,
                    'rules' => require('backend-rules.php'),
                ],
                'frontend' => [ // public side
                    'enablePrettyUrl' => true,
                    'showScriptName' => false,
                    'rules' => require('frontend-rules.php'),
                ],
            ],
            'defaultManager' => 'frontend'
        ],
        'log' => [
            'targets' => [
                'sentry' => [
                    'class' => \notamedia\sentry\SentryTarget::class, // for example
                    'levels' => ['error', 'warning'],
                    'clientOptions' => [],
                ]
            ]
        ]
    ],
    'container' => [
        'singletons' => [
            app\components\WalletCharger::class => []
        ]
    ]
];

return $config;
