<?php

namespace Tests\unit\modules\loyalty

use Codeception\Test\Unit;
use modules\core\common\components\Currency\Factory;
use modules\core\common\components\Settings;
use modules\loyalty\common\components\PointConverter;
use modules\payment\common\constants\CurrencyType;

/**
 * Class ConverterTest
 * @package Tests\unit\modules\loyalty
 */
class ConverterTest extends Unit
{
    /**
     * @dataProvider getDataForSimpleConverting
     *
     * @param $points
     * @param $exchangeRate
     * @param $currency
     * @param $expected
     */
    public function testSimpleConverting($points, $exchangeRate, $currency, $expected)
    {
        $setting = $this->createMock(Settings::class);
        $setting->method("getByTag")
            ->willReturn($exchangeRate);

        $converter = new PointConverter($setting);

        $this->assertEquals($expected, $converter->toAmount($points, $currency)->amount);
    }

    /**
     * @return array
     */
    public function getDataForSimpleConverting()
    {
        $points = [null, 0, 1, 10, 150, 1500];
        $rateList = [
            100,
            [CurrencyType::RUB_INT => 0.1],
            [CurrencyType::RUB_INT => 100],
            [CurrencyType::RUB_INT => 0.001],
        ];

        $data = [];
        $currency = Factory::rub(0);

        foreach($points as $point) {
            foreach($rateList as $rate) {
                $r = $rate[$currency->code] ?? $rate;

                $data[] = [
                    'points' => $point,
                    'exchangeRate' => $rate,
                    'currency' => $currency,
                    'expected' => $point * $r
                ];
            }
        }

        return $data;
    }
}
