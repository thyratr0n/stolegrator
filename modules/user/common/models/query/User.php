<?php

namespace modules\user\common\models;

use yii\db\ActiveQuery;

class User extends ActiveQuery
{
    /**
     * @param int $id
     * @return User
     */
    public function byId(int $id)
    {
        return $this->where('id = :id', [':id' => $id]);
    }
}