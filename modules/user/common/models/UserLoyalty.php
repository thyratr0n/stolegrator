<?php

namespace modules\user\common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $value
 * @property string $created_at
 * @property string $updated_at
 */
class UserLoyalty extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_loyalty';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            't' => [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }
}
