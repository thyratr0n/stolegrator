<?php

namespace modules\user\common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $phone
 * @property string $created_at
 * @property string $updated_at
 */
class User extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            't' => [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * @return query\User
     */
    public static function find()
    {
        return new query\User(get_called_class());
    }
}
