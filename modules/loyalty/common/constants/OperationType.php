<?php

namespace modules\loyalty\common\constants;

/**
 * Class OperationType
 * @package modules\loyalty\common\constants
 */
abstract class OperationType
{
    const TYPE_ADD_FROM_PRIZE = 'add_from_prize'; // generate randomly
    const TYPE_TRANSACT_TO_WALLET = 'transact_to_wallet'; // to user's wallet
}
