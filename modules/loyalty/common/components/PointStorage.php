<?php

namespace modules\loyalty\common\components;

use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class PointStorage
 * @package modules\loyalty\common\components
 */
class PointStorage extends Component
{
    /**
     * @param User $user
     * @param int $points
     * @return bool
     */
    public function storeToUser(User $user, int $points): bool
    {
        return true; // store points
    }
}
