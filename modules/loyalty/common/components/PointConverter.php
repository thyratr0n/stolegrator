<?php

namespace modules\loyalty\common\components;

use modules\core\common\components\Currency;
use modules\core\common\components\Settings;
use modules\loyalty\common\constants\OperationType;
use modules\payment\common\components\PaymentSystem;
use modules\payment\common\components\TransactionManager;
use modules\user\common\models\User;
use modules\user\common\models\UserLoyalty;
use modules\user\common\models\UserLoyaltyLog;
use yii\base\Component;

/**
 * Class PointConverter
 * @package modules\loyalty\common\components
 */
class PointConverter extends Component
{
    const SETTING_EXCHANGE_RATE = 'exchange_rate';
    const SETTING_EXCHANGE_DEFAULT_RATE = 'exchange_default_rate';

    /**
     * For getting exchange rate
     * @var Settings
     */
    protected $settings;

    /**
     * PointManager constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param int $points
     * @param Currency $currency
     * @return Currency
     */
    public function toAmount(int $points, Currency $currency): Currency
    {
        $currency->amount = $this->getExchangeRate($currency) * $points;

        return $currency;
    }

    /**
     * @param Currency $currency
     * @return int
     */
    public function fromAmount(Currency $currency): int
    {
        return floor($currency->amount / $this->getExchangeRate($currency)); // or round()
    }

    /**
     * @return float
     */
    public function getDefaultExchangeRate(): float
    {
        return $this->settings
            ->getByTag(static::SETTING_EXCHANGE_DEFAULT_RATE)
            ->value;
    }

    /**
     * @param Currency $currency
     * @return float
     */
    protected function getExchangeRate(Currency $currency): float
    {
        return $this->settings
            ->getByTag(static::SETTING_EXCHANGE_RATE)
            ->value[$currency->code] ?? $this->getDefaultExchangeRate();
    }
}
