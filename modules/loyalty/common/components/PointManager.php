<?php

namespace modules\loyalty\common\components;

use modules\core\common\components\Currency;
use modules\core\common\components\Settings;
use modules\loyalty\common\constants\OperationType;
use modules\payment\common\components\PaymentSystem;
use modules\payment\common\components\TransactionManager;
use modules\user\common\models\User;
use modules\user\common\models\UserLoyalty;
use modules\user\common\models\UserLoyaltyLog;
use yii\base\Component;

/**
 * Class PointManager
 * @package modules\loyalty\common\components
 */
class PointManager extends Component
{
    const SETTING_TAG_MIN = 'point_generate_min';
    const SETTING_TAG_MAX = 'point_generate_max';

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var PointConverter
     */
    protected $converter;

    /**
     * @var TransactionManager
     */
    protected $manager;

    /**
     * @var PointStorage
     */
    protected $storage;

    /**
     * PointManager constructor.
     * @param Settings $settings
     * @param PointConverter $converter
     * @param PointStorage $storage
     * @param TransactionManager $manager
     */
    public function __construct(
        Settings $settings,
        PointConverter $converter,
        PointStorage $storage,
        TransactionManager $manager
    ) {
        $this->settings = $settings;
        $this->converter = $converter;
        $this->manager = $manager;
        $this->storage = $storage;
    }

    /**
     * @param int $points
     * @param User $user
     * @param PaymentSystem $ps
     * @param Currency $currency
     * @return bool
     */
    public function transactToAmountForUser(int $points, User $user, PaymentSystem $ps, Currency $currency): bool
    {
        $currency = $this->converter->toAmount($points, $currency);

        // all transaction send through queue
        $this->manager->markForSend($ps, $user, $currency);

        return true;
    }

    /**
     * @param User $user
     * @param Currency $currency
     * @return bool
     */
    public function transactToPoints(User $user, Currency $currency): bool
    {
        $points = $this->converter->fromAmount($currency);

        return $this->storage->storeToUser($user, $points);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function getRandomCountForUser(User $user): bool
    {
        $min = $this->settings->getByTag(static::SETTING_TAG_MIN)->value;
        $max = $this->settings->getByTag(static::SETTING_TAG_MAX)->value;

        $pointValue = $this->generatePointCount($min, $max);

        return $this->addPointsToUser($user, $pointValue);
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    protected function generatePointCount(int $min, int $max): int
    {
        return rand($min, $max); // just for example
    }

    /**
     * @param User $user
     * @param int $poins
     * @return bool
     */
    protected function addPointsToUser(User $user, int $poins): bool
    {
        $loyalty = $this->getLoyaltyByUser($user);

        $loyalty->value += $poins;

        try {
            /**
             * Need store log first for infrastructure's errors catching
             */
            if (
                $this->storeLog(
                    $loyalty,
                    OperationType::TYPE_ADD_FROM_PRIZE,
                    $poins
                )
            ) {
                return $loyalty->save();
            }
        } catch (\yii\db\Exception $e) {
            /* catch DB exceptions*/
        }

        throw new WrongStateException('Unable to save generated');
    }

    /**
     * @param UserLoyalty $loyalty
     * @param string $operation
     * @param int $value
     * @return bool
     */
    protected function storeLog(UserLoyalty $loyalty, string $operation, int $value): bool
    {
        $log = new UserLoyaltyLog();
        $log->user_id = $loyalty->user_id;
        $log->operation = $operation;
        $log->value = $value;

        return $log->insert();
    }

    /**
     * @param User $user
     * @return UserLoyalty
     */
    protected function getLoyaltyByUser(User $user): UserLoyalty
    {
        $loyalty = UserLoyalty::find()->byUser($user);

        if (!$loyalty) {
            $loyalty = new UserLoyalty();
            $loyalty->user_id = $user->id;
        }

        return $user;
    }
}
