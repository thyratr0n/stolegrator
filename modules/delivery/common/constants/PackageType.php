<?php

namespace modules\delivery\common\constants;

/**
 * Class PackageType
 * @package modules\delivery\common\constants
 */
abstract class PackageType
{
    const TYPE_PRIZE = 'prize_package_variant';
}
