<?php

namespace modules\devivery\common\components;

use modules\core\common\components\Currency;
use yii\base\Component;

/**
 * Class PackageVariant
 * @package modules\devivery\common\components
 */
abstract class PackageVariant
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var Currency
     */
    public $cost;

    /**
     * @var float
     */
    public $weight;

    /**
     * PackageVariant constructor.
     * @param Currency $currency
     * @param float $weight
     * @param string $type
     */
    public function __construct(Currency $currency, float $weight, string $type)
    {
        $this->type = $type;
        $this->cost = $currency;
        $this->weight = $weight;
    }
}
