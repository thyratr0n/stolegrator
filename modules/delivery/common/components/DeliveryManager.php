<?php

namespace modules\devivery\common\components;

use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class DeliveryManager
 * @package modules\devivery\common\components
 */
class DeliveryManager extends Component
{
    /**
     * @param User $user
     * @param PackageVariant $package
     * @return bool
     */
    public function startToUser(User $user, PackageVariant $package): bool
    {
        // start delivery for user
    }
}
