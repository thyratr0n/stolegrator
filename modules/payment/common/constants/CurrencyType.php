<?php

namespace modules\payment\common\constants;

/**
 * Class CurrencyType
 * @package modules\payment\common\constants
 */
abstract class CurrencyType
{
    const RUB_INT = 643;
    const RUB_STR = 'rub';

}
