<?php

namespace modules\payment\common\components\PaymentSystem;

use modules\core\common\components\Currency;

/**
 * Class Gateway
 * @package modules\payment\common\components\PaymentSystem
 */
abstract class Gateway
{
    /**
     * In practice there may be Request/Response exchange between Gateway
     * and PaymentSystem components for handling and logging;
     *
     * @param Currency $currency
     * @return bool
     */
    abstract public function start(Currency $currency): bool;
}
