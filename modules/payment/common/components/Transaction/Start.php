<?php

namespace modules\payment\common\components\Transaction;

use modules\payment\common\components\Currency;
use modules\payment\common\components\PaymentSystem;
use modules\user\common\models\User;

class Start
{
    /**
     * @param User $user
     * @param PaymentSystem $ps
     * @param Currency $currency
     * @return bool
     */
    public function run(User $user, PaymentSystem $ps, Currency $currency): bool
    {
        return true;
    }
}
