<?php

namespace modules\payment\common\components;

class PaymentSystem
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $gateway;
}
