<?php

namespace modules\payment\common\components;

use modules\core\common\components\Currency;
use modules\payment\common\components\Transaction\Start;
use modules\payment\common\models\Transaction;
use modules\user\common\models\User;
use yii\base\Component;

class TransactionManager extends Component
{
    /**
     * @param User $user
     * @param Currency $currency
     * @return bool
     */
    public function start(PaymentSystem $ps, User $user, Currency $currency): bool
    {
        Event::trigger($this, 'Transaction starting', 'Some context');
        $step = new Start();

        try {
            $result = $step->run($user, $ps, $currency);
        } catch (TransactionException $e) {
            // Mark in case of suitable retriable
            $this->markForResend($ps, $user, $currency);

            return false;
        }

        Event::trigger($this, 'Prize finished', 'Some context');

        return $result;
    }

    /**
     * @param PaymentSystem $ps
     * @param User $user
     * @param Currency $currency
     * @return $this
     */
    public function markForSend(PaymentSystem $ps, User $user, Currency $currency): self
    {
        // store to sending queue
        return $this;
    }

    /**
     * @param Transaction $transaction
     * @return bool
     */
    public function restart(Transaction $transaction): bool
    {
        //restart sending
    }

    /**
     * @param Transaction $transaction
     * @return bool
     */
    public function markForResend(Transaction $transaction): bool
    {
        // mark transaction for resend
    }
}
