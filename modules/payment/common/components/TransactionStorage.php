<?php

namespace modules\payment\common\components;

use yii\base\Component;

class TransactionStorage extends Component
{
    /**
     * @param int $limit
     * @return array
     */
    public function getListForSend(int $limit): array
    {
        return []; // list of Transactions for send
    }
}
