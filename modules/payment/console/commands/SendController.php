<?php
namespace modules\payment\console\commands;

use modules\payment\common\components\TransactionManager;
use modules\payment\common\components\TransactionStorage;
use modules\payment\common\models\Transaction;
use yii\console\Controller;
use yii\console\ExitCode;

class SendController extends Controller
{
    /**
     * @var int
     */
    public $count = 20;

    /**
     * @var TransactionStorage
     */
    protected $storage;

    /**
     * @var TransactionManager
     */
    protected $manager;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return
            array_merge(
                parent::options($actionID),
                ['count']
            );
    }

    /**
     * SendController constructor.
     * @param TransactionStorage $storage
     * @param TransactionManager $manager
     */
    public function __construct(
        TransactionStorage $storage,
        TransactionManager $manager
    ) {
        $this->storage = $storage;
        $this->manager = $manager;
    }

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $list = $this->storage->getListForSend($this->count);

        /* @var $element Transaction */
        foreach($list as $element) {
            try {
                $this->manager->restart($element);
            } catch (RetryPossibleTransactionException $e) {
                $this->manager->markForResend($element);
            }
        }
    }
}
