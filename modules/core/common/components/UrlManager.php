<?php

namespace modules\core\common\components;

use yii\base\Component;

class UrlManager extends Component
{
    public $managers = [];
    public $defaultManager = '';

    /**
     * @var \yii\web\UrlManager[]
     */
    protected $initializedManagers = [];

    public function init()
    { /* managers initializing*/ }
}
