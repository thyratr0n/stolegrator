<?php

namespace modules\core\common\components;

class Currency
{
    /**
     * @var int
     */
    public $code;

    /**
     * @var string
     */
    public $codeString;

    /**
     * @var float
     */
    public $amount;

    /**
     * Currency constructor.
     * @param int $code
     * @param string $codeString
     * @param float $amount
     */
    public function __construct(int $code, string $codeString, float $amount)
    {
        $this->code = $code;
        $this->codeString = $codeString;
        $this->amount = $amount;
    }
}
