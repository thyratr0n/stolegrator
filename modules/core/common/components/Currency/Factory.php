<?php

namespace modules\core\common\components\Currency;

use modules\core\common\components\Currency;
use modules\core\common\constants\CurrencyType;

abstract class Factory
{
    /**
     * @param float $amount
     * @return Currency
     */
    public static function rub(float $amount): Currency
    {
        return new Currency(CurrencyType::RUB_INT, CurrencyType::RUB_STR, $amount);
    }
}
