<?php

namespace modules\prize\common\components;

use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class Variant
 * @package modules\prize\common\components
 */
abstract class Variant extends Component
{
    const TYPE = '';

    /**
     * @var bool
     */
    protected $isAvailable;

    /**
     * @param User $user
     * @return bool
     */
    abstract public function makeForUser(User $user): bool;

    /**
     * Message getting. Settings/DB/etc
     * @return string
     */
    public function getMessage(): string
    {
        return ''; // success message getting
    }

    /**
     * @return bool
     */
    public function isNotAvailable(): bool
    {
        if (is_null($this->isAvailable)) {
            $this->detectAvailability();
        }

        return !$this->isAvailable;
    }

    /**
     * @return $this
     */
    abstract protected function detectAvailability();
}
