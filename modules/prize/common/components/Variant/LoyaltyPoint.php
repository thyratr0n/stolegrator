<?php

namespace modules\prize\common\components\Variant;

use modules\loyalty\common\components\PointManager;
use modules\prize\common\components\Variant;
use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class LoyaltyPoint
 * @package modules\prize\common\components
 */
class LoyaltyPoint extends Variant
{
    const TYPE = 'loyalty_point';

    /**
     * @var PointManager
     */
    protected $loyaltyPointManager;

    /**
     * @var bool
     */
    protected $isAvailable = true;

    /**
     * LoyaltyPoint constructor.
     * @param PointManager $loyaltyPointManager
     */
    public function __construct(PointManager $loyaltyPointManager)
    {
        $this->loyaltyPointManager = $loyaltyPointManager;
    }

    /**
     * @inheritDoc
     */
    public function makeForUser(User $user): bool
    {
        return $this->loyaltyPointManager->getRandomCountForUser($user);
    }

    /**
     * @return $this
     */
    protected function detectAvailability()
    {
        return $this;
    }
}
