<?php

namespace modules\prize\common\components\Variant;

use modules\prize\common\components\Variant;
use modules\user\common\models\PrizeElement;
use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class Prize
 * @package modules\prize\common\components
 */
class Prize extends Variant
{
    const TYPE = 'prize';

    /**
     * @var PrizeManager
     */
    protected $prizeManager;

    /**
     * @var PrizeElement
     */
    protected $prizeElement;

    /**
     * LoyaltyPoint constructor.
     * @param PrizeManager $prizeManager
     */
    public function __construct(PrizeManager $prizeManager)
    {
        $this->prizeManager = $prizeManager;
    }

    /**
     * @inheritDoc
     */
    public function makeForUser(User $user): bool
    {
        if ($this->isAvailable) {
            $this->prizeManager = $this->prizeManager->getRandomForUser($user);

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->prizeElement
            ? "You won: {$this->prizeElement->name}!" // just for example
            : '';
    }

    /**
     * @return $this
     */
    protected function detectAvailability(): Variant
    {
        $this->isAvailable = $this->prizeManager->existsAvailable();

        return $this;
    }
}
