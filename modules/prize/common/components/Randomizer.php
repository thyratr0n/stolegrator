<?php

namespace modules\prize\common\components;

use modules\core\common\components\Settings;
use modules\prize\common\components\Variant\LoyaltyPoint;
use modules\user\common\models\User;
use yii\base\Component;
use yii\base\Event;

/**
 * Class Randomizer
 * @package modules\prize\common\components
 */
class Randomizer extends Component
{
    const SETTING_ALLOWED_LIST = 'allowed_list';

    /**
     * @var Settings
     */
    protected $settings;

    protected $variantList;

    /**
     * Randomizer constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param User $user
     * @return Result
     */
    public function makeForUser(User $user): Result
    {
        \Yii::info('Prize starting', 'prize');

        $rndKey = rand(0, count($this->variantList) - 1);

        /* @var $variant Variant */
        $variant = $this->variantList[$rndKey];

        if ($variant->makeForUser($user)) {
            \Yii::info('Prize success', 'prize');

            return  new Result(true, $variant->getMessage());
        }

        throw new WrongStateException('Unable to take random prize');
    }

    /**
     * @return array
     */
    protected function getVariantList(): array
    {
        if (is_null($this->variantList)) {
            $this->loadVariandList();
        }

        if (empty($this->variantList)) {
            throw new WrongStateException('Empty variant list');
        }

        return $this->variantList;
    }

    /**
     * @return $this
     */
    protected function loadVariandList(): self
    {
        $allowedList = $this->settings->getByTag(static::SETTING_ALLOWED_LIST)->value;

        $this->variantList[] = \Yii::createObject(LoyaltyPoint::class); // by allowed list

        /* @var $variant Variant */
        foreach($this->variantList as $key => $variant) {
            if ($variant->isNotAvailable()) {
                unset($this->variantList[$key]);

                \Yii::info('Variant is unable to use', 'prize');
            }
        }

        return $this;
    }
}
