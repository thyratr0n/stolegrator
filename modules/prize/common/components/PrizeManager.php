<?php

namespace modules\prize\common\components\Variant;

use modules\core\common\components\Settings;
use modules\user\common\models\PrizeElement;
use modules\user\common\models\User;
use yii\base\Component;

/**
 * Class PrizeManager
 * @package modules\prize\common\components
 */
class PrizeManager extends Component
{
    const SETTING_PRIZE_LIST = 'prize_list';

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * PrizeManager constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param User $user
     * @return PrizeElement
     */
    public function getRandomForUser(User $user): PrizeElement
    {
        return new PrizeElement();
    }

    /**
     * @return bool
     */
    public function existsAvailable(): bool
    {
        return true; // check for available list of prizes
    }
}
