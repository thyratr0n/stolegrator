<?php

namespace modules\prize\common\components;

/**
 * Class Result
 * @package modules\prize\common\components
 */
class Result extends Component
{
    /**
     * @var bool
     */
    public $isSuccess;

    /**
     * @var string
     */
    public $message;

    public function __construct(bool $isSuccess, string $message)
    {
        $this->isSuccess = $isSuccess;
        $this->message = $message;
    }
}
