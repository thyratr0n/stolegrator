<?php

namespace modules\personal\common\components;

use modules\user\common\models\User;
use yii\base\Component;

/**
 * Possible not only Prize, just Delivery.
 *
 * Class PrizeDelivery
 * @package modules\personal\common\components
 */
class PrizeDelivery extends Component
{
    /**
     * @param User $user
     * @return array
     */
    public function getDeliveryListForUser(User $user): array
    {
        return [];
    }
}
