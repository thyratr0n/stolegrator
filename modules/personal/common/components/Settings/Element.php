<?php

namespace modules\personal\common\components\Settings;

class Element
{
    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string | int | float
     */
    public $value;
}
