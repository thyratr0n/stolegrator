<?php

namespace modules\personal\common\components;

use yii\base\Component;

class Settings extends Component
{
    /**
     * @var Settings\Element[]
     */
    protected $list = [];

    /**
     * @param string $tag
     * @return Settings\Element|null
     */
    public function getByTag(string $tag): ?Settings\Element
    {
        return $this->list[$tag] ?? null;
    }
}
